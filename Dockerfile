FROM docker:stable

LABEL version="1.1"
LABEL description="This image is used to run the sustainabill CD pipelines.\
It is derived from docker:stable and adds curl as well as helpers to create a diff\
from the latest deployed version automatically"
LABEL maintainer="Thorsten Merten <thorsten.merten@sustainabill.de>"

# install CURL to enable automatic deployment to staging
# install GIT to enable compareversions script
RUN apk add --no-cache curl git

COPY compareversions.sh /usr/local/bin
RUN chmod +x /usr/local/bin/compareversions.sh

