# sustainabill CI-CD-runner

A docker image to run the sustainabill CD pipeline.

Basically a stable docker image with curl installed so that the GitLab CI pipeline can tell kubernetes to trigger a new deployment after a new image was build and pushed to the registry.


## how to use curl to update a deployment in GitLab

1. Set `K8S_TOKEN`, `K8S_URL`, and `SLACK_DEV_HOOK_KEY` as GitLab variable
  * (you may exchange the first line of the deploy script to whatever you need to do to trigger an deployment in you environment)
2. Add a deployment such as the following to your `.gitlab-ci.yaml` (everything in `<>` needs to be replaced)
```
my deployment:
  stage: deploy
  environment:
    name: <dev>
    url: <https://dev.example.com>
  script:
    - 'curl --header "Authorization: Bearer ${K8S_TOKEN}"
       --insecure
       --header "Content-Type: application/merge-patch+json"
       -X PATCH ${K8S_URL}/apis/apps/v1/namespaces/<NAMESPACE>/deployments/<DEPLOYMENT_NAME>
       --data "{\"spec\":{\"template\":{\"metadata\":{\"labels\":{\"date\":\"$(date +%s)\"}}}}}"'
    - sh /usr/local/bin/compareversions.sh ${CI_COMMIT_SHA} ${CI_PROJECT_URL} <https://example.com>
    - "curl -X POST -H 'Content-type: application/json' --data @slack.json https://hooks.slack.com/services/${SLACK_DEV_HOOK_KEY}"
    only:
    - <SOME_BRANCH>
```
3. The change of the template metadata triggers a redeployment if `ImagePullPolicy` is set to `always`)
* (you may also import your K8S certificate to GitLab, make it available as a file variable and refer to it in curl)


## Interesting GitLab variables for CI/CD

* `CI_PROJECT_NAME`: the project name
* `CI_COMMIT_SHA`: current commit
* `CI_COMMIT_BEFORE_SHA`: last commit to this branch
* `CI_COMMIT_REF_NAME`: where did we commit/push to?
* `CI_COMMIT_TAG`: for which tag was the build done?
