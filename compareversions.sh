#!/bin/sh
if [ -z $1 ]; then
  echo
  echo "assuming your project uses version tags (v.X.Y.Z) which can be sorted,"
  echo "this script compares the newest tag to the currently build version"
  echo
  echo "assuming you link your issues to you commits it will then find and output"
  echo "all issues you touched since the latest version (e.g. the latest version tag)."
  echo
  echo
  echo "usage:"
  echo "\$ $0 <commit-sha> <gitlab-project-path> <deployment-url>"
  echo "e.g."
  echo "\$ $0 0fd8a85c49be9d33ec71ee3746c7d22f32b99c36 https://gitlab.com/my-group/my-project https://myapp.example.com"
  echo
  exit 1
fi

# grab all tags in a pretty format (should be sorted descending by version)
# filter out everything matching a version: vX.Y.Z
# grab newest one (head -n 1)
NEWEST_VERSION_TAG=$(git log --tags --simplify-by-decoration --pretty="format:%ai %d" | grep -o "v[0-9]\+\.[0-9]\+\.[0-9]\+" | head -n 1)

# see https://stackoverflow.com/a/17744637 for more details
WORKING_DIR=$(cd -P -- "$(dirname -- "$0")" && pwd -P)

COMMIT_LOG=$(git log --pretty=oneline ${NEWEST_VERSION_TAG}..$1)
ISSUE_IDS=$(printf "$COMMIT_LOG" | grep -o "\#\d\+" | sort -u)

JSON="{\"@type\": \"MessageCard\",\"@context\": \"https://schema.org/extensions\",\"summary\":\"Update to test\",\"themeColor\":\"505394\",\"sections\":["
JSON="${JSON}{\"activityTitle\":\"${3} updated\",\"facts\":["
for i in ${ISSUE_IDS}; do
  # ${i:1} string from position 1, will remove the #
  LINE="${2}/issues/${i:1}"
  JSON="${JSON}{\"name\": \"${i:1}\",\"value\":\"${LINE}\"},"
done
JSON="${JSON%?}],"
JSON="${JSON}\"potentialAction\": [{\"@type\":\"OpenUri\",\"name\":\"Have a look\",\"targets\":[{\"os\":\"default\",\"uri\":\"${3}\"}]}]"
JSON="${JSON}}]}"

printf '%s' "${JSON}" > teams.json
